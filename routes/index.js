const express = require('express');
const router = express.Router();
const usersFile = '../data/users.json';
const users = require(usersFile);
const fs = require('fs');
const path = require('path');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

router.get('/authorization', (req, res) => {
  //get params from header
  let authHeader = req.headers.authorization;
  let auth = authHeader ? new Buffer.from(authHeader.split(' ')[1], 'base64').toString().split(':') : [];

  let login = auth[0],
    password = auth[1];

  if(users[login] && users[login].password === password){
    res.json({user: login});
  } else {
    res.status(401).json({"err": 401});
  }
});

router.post('/registration', (req, res) => {
  //get params from header
  let authHeader = req.headers.authorization;

  let auth = authHeader ? new Buffer.from(authHeader.split(' ')[1], 'base64').toString().split(':') : [];

  let login = auth[0],
    password = auth[1];

  if(users[login]){
    res.status(409).json({"err": 409});
    return;
  }

  users[login] = {
    password: password
  };

  fs.writeFile(path.resolve("./data/users.json"), JSON.stringify(users, null, 2), function (err) {
    if (err) return console.log(err);
    console.log(JSON.stringify(users));
    console.log('writing to ' + usersFile);
  });

  let meta = {
    created: new Date().getTime(),
    changed: new Date().getTime()
  };

  let template = {
    meta: meta,
    words: []
  };

  fs.writeFile(path.resolve(`./data/${login}.json`), JSON.stringify(template, null, 2), function (err) {
    if (err) return console.log(err);
    console.log(JSON.stringify(template));
    res.json({user: login})
  });
});

router.get('/read', (req, res) => {
  let user = req.query.user;
  let userData = require(`../data/${user}.json`);

  let userWords = userData ? userData.words : [];
  res.json(userWords);
});

router.post('/update', (req, res) => {
  if(req.query.user === req.cookies.user){
    let user = req.query.user;
    let userData = require(`../data/${user}.json`);
    userData.words = req.body.slice();

    fs.writeFile(`./data/${user}.json`, JSON.stringify(userData, null, 2), (err => {
      if (err) return console.log(err);
      console.log("userData", JSON.stringify(userData));
      res.json({update: true});
    }));
  } else {
    res.status(403).json({err: 403});
  }
});

module.exports = router;
